// scale.h

#ifndef SCALE_H
#define SCALE_H


const int LOADCELL_VOLTAGE_PIN_1 = 5;                      // update gpio pins as needed
const int LOADCELL_VOLTAGE_PIN_2 = 6;
const float LOADCELL_SLOPE_1 = 1.0949;
const float LOADCELL_INTERCEPT_1 = 0.7179;                     //TODO get the lines of best fit from Jake
const float LOADCELL_SLOPE_2 = 1.1064;
const float LOADCELL_INTERCEPT_2 = 0.4376;

extern float LOADCELL_ZERO_VOLTAGE_1;
extern float LOADCELL_ZERO_VOLTAGE_2;


float getStableWeight();
float testgetStableWeight();
float findStableValue(float* values, int numValues);
void zeroScale();
#endif