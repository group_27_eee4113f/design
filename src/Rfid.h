// Rfid.h
#ifndef RFID_H
#define RFID_H

#include <Arduino.h>



class Rfid {
public:
    Rfid();
    void init();
    bool readCard();
    String getCardId();
private:
    String _cardId;
    char read();
    bool isHexadecimalDigit(char c);
};

#endif