// filters.cpp

#include "filters.h"
#include <Arduino.h>
#include <algorithm>
#include <vector>
#include <numeric>
#include "SD_card.h"

//int windowSize = 10;      // use for a constant window size




float medianFilter(const std::vector<float>& readings, int windowSize) {
    std::vector<float> filteredReadings(readings.size());
    int index = 0;

    for (int i = 0; i < readings.size(); i++) {
        std::vector<float> window;
        window.reserve(windowSize);

        // Fill the window vector with the current reading and its neighbors
        for (int j = i - windowSize / 2; j <= i + windowSize / 2; j++) {
            if (j >= 0 && j < readings.size()) {
                window.push_back(readings[j]);
            }
        }

        // Sort the window vector and get the median value
        std::sort(window.begin(), window.end());
        filteredReadings[index++] = window[window.size() / 2];
    }

    // Calculate the average of the filtered readings
    float sum = std::accumulate(filteredReadings.begin(), filteredReadings.end(), 0.0f);
    float average = sum / filteredReadings.size();

    return average;
}


/*
// Moving Average Filter
float movingAverageFilter(float* readings, int numReadings, int windowSize) {
    float* filteredReadings = new float[numReadings];
    int index = 0;

    for (int i = 0; i < numReadings; i++) {
        float sum = 0;
        int count = 0;

        // Calculate the sum of the window for each reading
        for (int j = i - windowSize / 2; j <= i + windowSize / 2; j++) {
            if (j >= 0 && j < numReadings) {
                sum += readings[j];
                count++;
            }
        }

        // Calculate the moving average for the current reading
        filteredReadings[index++] = sum / count;
    }

    // Calculate the average of the filtered readings
    float sum = 0;
    for (int i = 0; i < numReadings; i++) {
        sum += filteredReadings[i];
    }
    float average = sum / numReadings;

    // Free the dynamically allocated memory for the filtered readings array
    delete[] filteredReadings;

    return average;
}*/




// // Kalman Filter
// float kalmanEstimate = 0;
// float kalmanError = 1;
// float kalmanGain = 0;
// float processNoise = 0.01;
// float measurementNoise = 0.1;

// float kalmanFilter(float measurement) {
//   // Predict
//   float predictedEstimate = kalmanEstimate;
//   float predictedError = kalmanError + processNoise;

//   // Update
//   kalmanGain = predictedError / (predictedError + measurementNoise);
//   kalmanEstimate = predictedEstimate + kalmanGain * (measurement - predictedEstimate);
//   kalmanError = (1 - kalmanGain) * predictedError;

//   return kalmanEstimate;
// }

// Test Filters
/*void testFilters(float* rawVoltages, int numReadings, const char* filename) {
  // Filtered readings arrays
  float movingAverageReadings[numReadings];
  float medianReadings[numReadings];
  float kalmanReadings[numReadings];

  // Apply filters
  for (int i = 0; i < numReadings; i++) {
    movingAverageReadings[i] = movingAverageFilter(rawVoltages, numReadings, 10);
    medianReadings[i] = medianFilter(rawVoltages, numReadings, 5);
    kalmanReadings[i] = kalmanFilter(rawVoltages[i]);
  }

  // Write the raw voltages and filtered readings to the SD card
  File dataFile = SD.open(filename, FILE_WRITE);
  if (dataFile) {
    // Write header row
    dataFile.println("Raw,Moving Average,Median,Kalman");

    // Write data rows
    for (int i = 0; i < numReadings; i++) {
      dataFile.print(rawVoltages[i]);
      dataFile.print(",");
      dataFile.print(movingAverageReadings[i]);
      dataFile.print(",");
      dataFile.print(medianReadings[i]);
      dataFile.print(",");
      dataFile.println(kalmanReadings[i]);
    }

    dataFile.close();
    Serial.println("Filter data written to SD card.");
  } else {
    Serial.println("Failed to open file for writing filter data.");
  }

  // Print the filtered results
  for (int i = 0; i < numReadings; i++) {
    Serial.print("Raw: ");
    Serial.print(rawVoltages[i]);
    Serial.print(", Moving Average: ");
    Serial.print(movingAverageReadings[i]);
    Serial.print(", Median: ");
    Serial.print(medianReadings[i]);
    Serial.print(", Kalman: ");
    Serial.println(kalmanReadings[i]);
  }
}*/