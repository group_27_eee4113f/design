#ifndef TEMPERATURE_H
#define TEMPERATURE_H

const int ONE_WIRE_BUS_1 = 4;
const int ONE_WIRE_BUS_2 = 7;

void initTemperatureSensor();
float getTemperature(int pin);

#endif