#include "temperature.h"
#include <OneWire.h>
#include <DallasTemperature.h>

OneWire oneWire1(ONE_WIRE_BUS_1);
OneWire oneWire2(ONE_WIRE_BUS_2);
DallasTemperature sensors1(&oneWire1);
DallasTemperature sensors2(&oneWire2);

void initTemperatureSensor() {
  sensors1.begin();
  sensors2.begin();
}

float getTemperature(int pin) {
  if (pin == ONE_WIRE_BUS_1) {
    sensors1.requestTemperatures();
    return sensors1.getTempCByIndex(0);
  } else if (pin == ONE_WIRE_BUS_2) {
    sensors2.requestTemperatures();
    return sensors2.getTempCByIndex(0);
  }
  return 0.0;
}