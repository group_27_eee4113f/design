#include "scale.h"
#include <Arduino.h>
#include <SimpleKalmanFilter.h>
#include <driver/adc.h>
#include <esp_adc_cal.h>
#include "filters.h"
#include <vector>
#include <numeric>


const int NUM_READINGS = 50;
const int READING_DELAY_MS = 50;
float LOADCELL_ZERO_VOLTAGE_1 = 0.0;
float LOADCELL_ZERO_VOLTAGE_2 = 0.0;

// ADC1 channels connected to the load cells
const adc1_channel_t LOADCELL_ADC_CHANNEL_1 = ADC1_CHANNEL_4;
const adc1_channel_t LOADCELL_ADC_CHANNEL_2 = ADC1_CHANNEL_5;

// ADC1 calibration variables

esp_adc_cal_characteristics_t adc1_chars;


float calculateStandardDeviation(float* values, int numReadings, float mean) {
    float sumOfSquaredDifferences = 0.0;
    for (int i = 0; i < numReadings; i++) {
        float difference = values[i] - mean;
        sumOfSquaredDifferences += difference * difference;
    }
    float variance = sumOfSquaredDifferences / (numReadings - 1);
    return sqrt(variance);
}


void zeroScale() {
  // Collect voltage readings when the scale is empty
  float zeroVoltages1[NUM_READINGS];
  float zeroVoltages2[NUM_READINGS];

  for (int i = 0; i < NUM_READINGS; i++) {
    uint32_t raw_voltage1 = adc1_get_raw(LOADCELL_ADC_CHANNEL_1);
    uint32_t raw_voltage2 = adc1_get_raw(LOADCELL_ADC_CHANNEL_2);
    float voltage1 = esp_adc_cal_raw_to_voltage(raw_voltage1, &adc1_chars) / 1000.0;
    float voltage2 = esp_adc_cal_raw_to_voltage(raw_voltage2, &adc1_chars) / 1000.0;
    zeroVoltages1[i] = voltage1;
    zeroVoltages2[i] = voltage2;
    delay(READING_DELAY_MS);
  }

  // Calculate the average zero voltages
  float sumZero1 = 0.0;
  float sumZero2 = 0.0;
  for (int i = 0; i < NUM_READINGS; i++) {
    sumZero1 += zeroVoltages1[i];
    sumZero2 += zeroVoltages2[i];
  }
  float zeroVoltage1 = sumZero1 / NUM_READINGS;
  float zeroVoltage2 = sumZero2 / NUM_READINGS;

  // Store the zero voltages
  LOADCELL_ZERO_VOLTAGE_1 = zeroVoltage1;
  LOADCELL_ZERO_VOLTAGE_2 = zeroVoltage2;

  Serial.print("Zero voltage 1: ");
  Serial.println(LOADCELL_ZERO_VOLTAGE_1);
  Serial.print("Zero voltage 2: ");
  Serial.println(LOADCELL_ZERO_VOLTAGE_2);
}


void initScaleADC() {
    // Configure the GPIO pins for ADC1
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(LOADCELL_ADC_CHANNEL_1, ADC_ATTEN_DB_11);
    adc1_config_channel_atten(LOADCELL_ADC_CHANNEL_2, ADC_ATTEN_DB_11);

    // Calibrate ADC1
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc1_chars);
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        Serial.println("ADC1 calibrated using eFuse Vref");
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        Serial.println("ADC1 calibrated using Two Point Value");
    } else {
        Serial.println("ADC1 calibrated using Default Vref");
    }
}



float getStableWeight() {
    initScaleADC();

    // Use std::vector for storing the voltage readings
    std::vector<float> voltages1;
    std::vector<float> voltages2;
    voltages1.reserve(NUM_READINGS);
    voltages2.reserve(NUM_READINGS);

    // Take multiple voltage readings from each load cell and store them in separate vectors
    for (int i = 0; i < NUM_READINGS; i++) {
        uint32_t raw_voltage1 = adc1_get_raw(LOADCELL_ADC_CHANNEL_1);
        uint32_t raw_voltage2 = adc1_get_raw(LOADCELL_ADC_CHANNEL_2);
        float voltage1 = esp_adc_cal_raw_to_voltage(raw_voltage1, &adc1_chars) / 1000.0;
        float voltage2 = esp_adc_cal_raw_to_voltage(raw_voltage2, &adc1_chars) / 1000.0;
        voltages1.push_back(voltage1);
        voltages2.push_back(voltage2);
        delay(READING_DELAY_MS);
    }

    // Apply median filter to the voltage readings
    int windowSize = 10; // Adjust the window size as needed
    float filteredVoltage1 = medianFilter(voltages1, windowSize);
    float filteredVoltage2 = medianFilter(voltages2, windowSize);

    // Calculate the weights using the filtered voltages
    float weight1 = filteredVoltage1 * LOADCELL_SLOPE_1 + LOADCELL_INTERCEPT_1;
    float weight2 = filteredVoltage2 * LOADCELL_SLOPE_2 + LOADCELL_INTERCEPT_2;

    // Calculate the total weight
    float totalWeight = weight1 + weight2;

    return totalWeight;
}











float testgetStableWeight() {
  initScaleADC();
  float voltages1[NUM_READINGS];
  float voltages2[NUM_READINGS];
  float filteredVoltages1[NUM_READINGS];
  float filteredVoltages2[NUM_READINGS];
  float sum_raw1 = 0;
  float sum_raw2 = 0;
  // Take multiple voltage readings from each load cell and store them in separate buffers
    for (int i = 0; i < NUM_READINGS; i++) {
        uint32_t raw_voltage1 = adc1_get_raw(LOADCELL_ADC_CHANNEL_1);
        uint32_t raw_voltage2 = adc1_get_raw(LOADCELL_ADC_CHANNEL_2);
        float voltage1 = esp_adc_cal_raw_to_voltage(raw_voltage1, &adc1_chars) / 1000.0;
        float voltage2 = esp_adc_cal_raw_to_voltage(raw_voltage2, &adc1_chars) / 1000.0;
        voltages1[i] = voltage1;
        voltages2[i] = voltage2;
        sum_raw1 += raw_voltage1;
        sum_raw2 += raw_voltage2;
        // Display the raw ADC values and calibrated voltages
        // Serial.print("Raw ADC1: ");
        // Serial.print(raw_voltage1);
        // Serial.print(", Voltage1: ");
        // Serial.print(voltage1);
        // Serial.print("V, Raw ADC2: ");
        // Serial.print(raw_voltage2);
        // Serial.print(", Voltage2: ");
        // Serial.print(voltage2);
        // Serial.println("V");
        delay(READING_DELAY_MS);
    }
    int raw_average1 = (int)(sum_raw1 / NUM_READINGS);
    int raw_average2 = (int)(sum_raw2 / NUM_READINGS);

    float sum_1 = 0.0;
    float sum_2 = 0.0;
    for (int i = 0; i < NUM_READINGS; i++) {
      sum_1 += voltages1[i];
      sum_2 += voltages2[i];
    }
    float average_1 = sum_1 / NUM_READINGS;
    float average_2 = sum_2 / NUM_READINGS;
    

  // Calculate standard deviations
    float stddev_1 = calculateStandardDeviation(voltages1, NUM_READINGS, average_1);
    float stddev_2 = calculateStandardDeviation(voltages2, NUM_READINGS, average_2);
    Serial.print("Raw Average 1 :");
    Serial.print(raw_average1);
    Serial.print(" Average 1 :");
    Serial.print(average_1,4);
    Serial.print(", Standard Deviation 1: ");
    Serial.println(stddev_1,4);
    Serial.print("Raw Average 2 :");
    Serial.print(raw_average2);
    Serial.print(" Average 2 :");
    Serial.print(average_2,4);
    Serial.print(", Standard Deviation 2: ");
    Serial.println(stddev_2,4);

    float pre_filt_weight1 = average_1*LOADCELL_SLOPE_1 + LOADCELL_INTERCEPT_1;
    float pre_filt_weight2 = average_2*LOADCELL_SLOPE_2 + LOADCELL_INTERCEPT_2;

    float pre_filt_totalWeight = pre_filt_weight1 + pre_filt_weight2;

    Serial.print("Pre filtered weight (average1)*Y1 :");
    Serial.println(pre_filt_weight1,4);
    Serial.print("Pre filtered weight (average2)*Y2 :");
    Serial.println(pre_filt_weight2,4);
    Serial.print("Pre filtered total weight :");
    Serial.println(pre_filt_totalWeight,4);

     // Test filters for load cell 1
  Serial.println("Filter testing for load cell 1:");
  testFilters(voltages1, NUM_READINGS, "/loadcell1_filters.csv");

  // Test filters for load cell 2
  Serial.println("Filter testing for load cell 2:");
  testFilters(voltages2, NUM_READINGS,"/loadcell2_filters.csv");


  // Apply Kalman filter to the voltage readings of each load cell independently
  SimpleKalmanFilter kalmanFilter1(0.1, 0.1, 0.01);
  SimpleKalmanFilter kalmanFilter2(0.1, 0.1, 0.01);
  for (int i = 0; i < NUM_READINGS; i++) {
    filteredVoltages1[i] = kalmanFilter1.updateEstimate(voltages1[i] - LOADCELL_ZERO_VOLTAGE_1);
    filteredVoltages2[i] = kalmanFilter2.updateEstimate(voltages2[i] - LOADCELL_ZERO_VOLTAGE_2);
  }

  // Find the most stable and accurate filtered voltage for each load cell
  float stableVoltage1 = findStableValue(filteredVoltages1, NUM_READINGS);
  float stableVoltage2 = findStableValue(filteredVoltages2, NUM_READINGS);

  // Apply the line of best fit to the stable voltages to obtain the weights
  float weight1 = stableVoltage1 * LOADCELL_SLOPE_1 + LOADCELL_INTERCEPT_1;
  float weight2 = stableVoltage2 * LOADCELL_SLOPE_2 + LOADCELL_INTERCEPT_2;

  // Add the weights to get the total weight
  float totalWeight = weight1 + weight2;
  Serial.print("Total weight = ");
  Serial.println(totalWeight);
  return totalWeight;
}


float findStableValue(float* values, int numValues) {
  // Find the most stable value from an array of values

  float sum = 0.0;
  for (int i = 0; i < numValues; i++) {
    sum += values[i];
  }
  float average = sum / numValues;
  return average;
}