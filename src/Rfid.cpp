// Rfid.cpp
#include "Rfid.h"
#include <HardwareSerial.h>


#define RX_PIN 44 
#define TX_PIN 43 

Rfid::Rfid() {
}

void Rfid::init() {
    //_rfidSerial->begin(9600);
    Serial2.begin(9600, SERIAL_8N1, RX_PIN, TX_PIN);
}

bool Rfid::readCard() {
    _cardId = "";
    char c;
    while (true) {
        c = read();
        if (c == -1) break;
        delay(5);
        if (c == 0x02) {  // Check if the character is the start marker (0x02)
            for (int i = 0; i < 12; i++) {
                c = read();
                Serial.print(c);
                if (c == -1) break;
                if (isHexadecimalDigit(c)) {
                    _cardId += c;
                    //Serial.print(_cardId);
                } else {
                    // Invalid character, discard the entire card ID
                    _cardId = "";
                    break;
                }
            }
            if (_cardId.length() == 12) {
                // Valid card ID received
                return true;
            }
        }
    }
    return false;
}

char Rfid::read() {
    if (Serial2.available()) {
        return Serial2.read();
    }
    return -1;  // Return -1 if no data is available
}

String Rfid::getCardId() {
    return _cardId;
}
bool Rfid::isHexadecimalDigit(char c) {
    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f') || (c == '/0');
}