#ifndef SDCARD_H
#define SDCARD_H

#include <SD.h>

void initSDCard();
void writeDataToSD(float temperature, float weight);
void printSDCardDetails();
void listFiles(const char * dirname, uint8_t levels);
void printSDCardUsage();

#endif