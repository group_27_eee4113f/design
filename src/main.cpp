#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include "scale.h"
#include "temperature.h"
#include "SD_card.h"
#include "Rfid.h"
#include "time.h"
#include <DS3231.h>
#include <Wire.h>



// WiFi credentials
const char* ssid = "dylan_esp";
const char* password = "password";

AsyncWebServer server(80);

// Dylans code for Micro controller system testing


// Rfid reader
Rfid rfid;      // create an instance of the RFID reader
DS3231 rtc;

// HTML for the web page
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
  <title>ESP32 Sensor Data</title>
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 1.5rem;}
    button {font-size: 1.5rem; padding: 10px 20px;}
  </style>
</head>
<body>
  <h2>ESP32 Sensor Data</h2>
  <p>Temperature: <span id="temperature">%TEMPERATURE%</span> &deg;C</p>
  <p>Weight: <span id="weight">%WEIGHT%</span> kg</p>
  <p>RFID Card ID: <span id="rfidCardId">%RFID_CARD_ID%</span></p>
  <p><button onclick="requestData()">Update Data</button></p>
  <p><button onclick="requestRfidData()">Update RFID Data</button></p>
  <script>
    function requestData() {
      fetch('/data')
        .then(response => response.json())
        .then(data => {
          document.getElementById('temperature').textContent = data.temperature;
          document.getElementById('weight').textContent = data.weight;
        });
    }
    function requestRfidData() {
            fetch('/rfid')
            .then(response => response.json())
            .then(data => {
                document.getElementById('rfidCardId').textContent = data.cardId;
            });
    }
  </script>
</body>
</html>
)rawliteral";

String processor(const String& var) {
  if (var == "TEMPERATURE") {
    float temperature = getTemperature();
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println(" °C");
    return String(temperature);
  }
  if (var == "WEIGHT") {
    float totalWeight = getStableWeight();
    Serial.print("Weight: ");
    Serial.print(totalWeight);
    Serial.println(" Kg");
    return String(totalWeight);
  }
  if (var == "RFID_CARD_ID") {
        String cardId = rfid.getCardId();
        Serial.print("RFID Card ID: ");
        Serial.println(cardId);
        return cardId;
    }
  return String();
}

void setup() {
  Serial.begin(9600);
  delay(1000);
  Wire.begin();
  rtc.begin();
  // Configure time synchronization settings
  configTime(0, 0, "pool.ntp.org");
  initSDCard();
  initTemperatureSensor();
  zeroScale();
  rfid.init();  // Initialize the Rfid object

  // Connect to WiFi
  Serial.println("Connecting to WiFi...");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected successfully!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Serve the HTML page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    String html = index_html;
    html.replace("%TEMPERATURE%", processor("TEMPERATURE"));
    html.replace("%WEIGHT%", processor("WEIGHT"));
    html.replace("%RFID_CARD_ID%", processor("RFID_CARD_ID"));
    request->send(200, "text/html", html);
  });

  // Handle data request
  server.on("/data", HTTP_GET, [](AsyncWebServerRequest *request){
    float temperature = getTemperature();
    float totalWeight = getStableWeight();
    String response = "{\"temperature\":" + String(temperature) + ",\"weight\":" + String(totalWeight) + "}";
    request->send(200, "application/json", response);
  });

  // Handle RFID data request
    server.on("/rfid", HTTP_GET, [](AsyncWebServerRequest *request){
        String cardId = rfid.getCardId();
        String response = "{\"cardId\":\"" + cardId + "\"}";
        request->send(200, "application/json", response);
    });

  // Start the server
  server.begin();
  Serial.println("Web server started.");

  /////

  Serial.println("WiFi connected successfully!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Get the current time from the NTP server
  time_t now;
  struct tm timeinfo;
  while (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time. Retrying...");
    delay(1000);
  }
  time(&now);

  // Set the RTC time with the current time
  rtc.setDateTime(timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday,
                  timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);


}

void loop() {
  delay(100);
   if (rfid.readCard()) {
        String cardId = rfid.getCardId();
        Serial.print("Card ID: ");
        Serial.println(cardId);
        // Process the card ID as needed


        // Check if weight is above threshold
    float totalWeight = getStableWeight();
    if (totalWeight > 0.5) {  // Adjust the threshold as needed
      // Perform measurements
      float temperature1 = getTemperature(ONE_WIRE_BUS_1);
      float temperature2 = getTemperature(ONE_WIRE_BUS_2);
      float weight = getStableWeight();

      // Write data to SD card
      writeDataToSD(temperature1, temperature2, weight);

      // Loop back
      return;
    }
   }
   // Deep sleep for 10 seconds
  esp_sleep_enable_timer_wakeup(10 * 1000000);
  esp_deep_sleep_start();
}
