// filters.h

#ifndef FILTERS_H
#define FILTERS_H
#include <vector>

float movingAverageFilter(float* readings, int numReadings, int windowSize);
float medianFilter(const std::vector<float>& readings, int windowSize);
float kalmanFilter(float measurement);
void testFilters(float* rawVoltages, int numReadings, const char* filename);

#endif