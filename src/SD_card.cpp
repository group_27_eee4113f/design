#include "SD_card.h"
#include <SPI.h>

File dataFile;

void initSDCard() {
  // Initialize SD card
  if (!SD.begin()) {
    Serial.println("SD card initialization failed!");
    return;
  }

  // Print SD card details
  printSDCardDetails();
  
  // Open the file for writing
  dataFile = SD.open("/data.csv", FILE_WRITE);
  if (!dataFile) {
    Serial.println("Failed to open data.csv for writing!");
    return;
  }
  
  // Write the header row
  dataFile.println("Temperature,Weight");
  dataFile.close();
}

void writeDataToSD(float temperature, float weight) {
  // Open the file for appending
  dataFile = SD.open("/data.csv", FILE_APPEND);
  if (dataFile) {
    // Write the temperature and weight data to the file
    dataFile.print(temperature);
    dataFile.print(",");
    dataFile.println(weight);
    dataFile.close();
    Serial.println("Data written to SD card.");
  } else {
    Serial.println("Failed to open data.csv for appending!");
  }
}

void printSDCardDetails() {
  uint8_t cardType = SD.cardType();
  Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
    Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);
}

void listFiles(const char * dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = SD.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels) {
        listFiles(file.path(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}

void printSDCardUsage() {
  Serial.printf("Total space: %lluMB\n", SD.totalBytes() / (1024 * 1024));
  Serial.printf("Used space: %lluMB\n", SD.usedBytes() / (1024 * 1024));
}