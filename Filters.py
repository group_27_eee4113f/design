import numpy as np
import matplotlib.pyplot as plt
from pykalman import KalmanFilter

def calculate_average(data):
    if len(data) == 0:
        return None
    
    data_sum = sum(data)
    average = data_sum / 50
    return average

def calculate_weight(V1, V2):
    #Y1 = 1.0949x + 0.7179
    #Y2 = 1.1064x + 0.4376
    weight1 = 1.0949 * V1 + 0.7179
    weight2 = 1.1064 * V2 + 0.4376
    total_weight = weight1 + weight2
    return total_weight

def moving_average_filter(data, window_size):
    filtered_data = []
    for i in range(len(data)):
        start = max(0, i - window_size + 1)
        window = data[start:i+1]
        avg = sum(window) / len(window)
        filtered_data.append(avg)
    return filtered_data

def exponential_moving_average_filter(data, alpha):
    filtered_data = [data[0]]
    for i in range(1, len(data)):
        filtered_data.append(alpha * data[i] + (1 - alpha) * filtered_data[-1])
    return filtered_data

def median_filter(data, window_size):
    filtered_data = []
    for i in range(len(data)):
        start = max(0, i - window_size // 2)
        end = min(len(data) - 1, i + window_size // 2)
        window = sorted(data[start:end+1])
        median = window[len(window) // 2]
        filtered_data.append(median)
    return filtered_data

def kalman_filter(data, process_noise, measurement_noise):
    estimated_voltage = data[0]
    error_estimate = 1.0
    filtered_data = []
    
    for voltage in data:
        # Prediction step
        predicted_voltage = estimated_voltage
        predicted_error = error_estimate + process_noise
        
        # Update step
        kalman_gain = predicted_error / (predicted_error + measurement_noise)
        estimated_voltage = predicted_voltage + kalman_gain * (voltage - predicted_voltage)
        error_estimate = (1 - kalman_gain) * predicted_error
        
        filtered_data.append(estimated_voltage)
    
    return filtered_data

# Apply the pykalman Kalman filter
def pykalman_filter(data, transition_matrix, measurement_matrix, initial_state_mean,
                    measurement_noise_covariance, process_noise_covariance):
    kf = KalmanFilter(transition_matrices=transition_matrix, observation_matrices=measurement_matrix,
                      initial_state_mean=initial_state_mean, observation_covariance=measurement_noise_covariance,
                      transition_covariance=process_noise_covariance)
    
    data_array = np.array(data)  # Convert the data list to a NumPy array
    measurements = data_array.reshape(len(data_array), 1)
    (filtered_state_means, filtered_state_covariances) = kf.filter(measurements)
    filtered_data = filtered_state_means[:, 0]
    
    return filtered_data

# Generate sample ADC voltage data with noise
sample_size = 50
time = np.linspace(0, 50)
# voltage_noise_1 = np.sin(2 * np.pi * 5 * time) + 0.15 * np.random.normal(size=sample_size)


# Test 1
# voltage_noise_2 = [1.68,
# 0,
# 1.81,
# 0,
# 1.02,
# 1.37,
# 1.85,
# 1.31,
# 1.54,
# 1.42,
# 1.77,
# 0,
# 1.4,
# 2.06,
# 1.12,
# 1.4,
# 1.53,
# 2.13,
# 0.96,
# 1.45,
# 1.63,
# 1.77,
# 1.21,
# 1.44,
# 2,
# 0.98,
# 1.27,
# 1.62,
# 1.79,
# 1.13,
# 1.54,
# 1.99,
# 1.14,
# 1.48,
# 1.37,
# 2.08,
# 0.75,
# 1.49,
# 1.15,
# 1.89,
# 0.89,
# 1.24,
# 1.85,
# 1.48,
# 1.48,
# 0.93,
# 2.01,
# 0.76,
# 1.32,
# 1.63,
# ]


# voltage_noise_1 = [1.03,
# 1.19,
# 1.21,
# 1.22,
# 0.92,
# 1.55,
# 1.27,
# 1.01,
# 1.64,
# 1.06,
# 1.12,
# 1.34,
# 1.07,
# 1.34,
# 0.97,
# 1.49,
# 0.82,
# 1.46,
# 0.76,
# 1.35,
# 0.66,
# 1.24,
# 1.32,
# 1.19,
# 1.15,
# 0.83,
# 1.34,
# 0.65,
# 1.17,
# 1.37,
# 1.18,
# 1.18,
# 0.88,
# 1.75,
# 0.94,
# 1.49,
# 0.65,
# 1.62,
# 0.69,
# 1.46,
# 0.96,
# 1.19,
# 1.09,
# 0.94,
# 1.95,
# 1.15,
# 1.67,
# 0.25,
# 1.48,
# 0.89,
# ]

#Test2
# voltage_noise_1 = [0.97,
# 1.28,
# 0,
# 0.94,
# 2.21,
# 1.09,
# 2.71,
# 1.89,
# 1.65,
# 0,
# 1.17,
# 1.81,
# 1.52,
# 0,
# 1.08,
# 1.19,
# 0.69,
# 1.24,
# 1.8,
# 0.04,
# 0,
# 2.04,
# 0,
# 0.88,
# 0,
# 1.14,
# 1.22,
# 2.06,
# 1.04,
# 1.63,
# 1.07,
# 2.08,
# 0,
# 1.45,
# 2.19,
# 1.44,
# 0,
# 0.42,
# 0,
# 1.39,
# 1.31,
# 0.63,
# 1.58,
# 1.45,
# 1.62,
# 1.16,
# 0.69,
# 0.03,
# 0.85,
# 1.73,
# ]

# voltage_noise_2 = [0.93,
# 1.62,
# 0.53,
# 1.31,
# 2.09,
# 1.15,
# 2.08,
# 1.9,
# 1.43,
# 0.66,
# 1.4,
# 2.1,
# 1.66,
# 0.76,
# 1.09,
# 1.99,
# 1.39,
# 1.34,
# 1.61,
# 0.7,
# 0.64,
# 1.72,
# 2.11,
# 1.11,
# 1.1,
# 1.43,
# 1.42,
# 1.72,
# 1.22,
# 1.83,
# 1.01,
# 1.68,
# 0.12,
# 1.34,
# 1.8,
# 1.34,
# 0.76,
# 1.11,
# 2.54,
# 1.42,
# 1,
# 0.68,
# 1.83,
# 1.56,
# 1.43,
# 1.05,
# 0.99,
# 1.26,
# 1.01,
# 1.15,
# ]

# #Test3

voltage_noise_1 = [0.93,
1.02,
0.92,
1.48,
0.96,
1.02,
0.93,
1.48,
0.71,
1,
0.86,
1.37,
0.39,
0.88,
0.71,
0.94,
0.02,
0.75,
0.79,
0.99,
0.72,
0.83,
0.83,
0.92,
0.85,
0.91,
0.89,
0.91,
0.95,
1.58,
1.02,
1.09,
1,
0.9,
1.02,
0.9,
0.91,
0.82,
0.83,
0.81,
0.76,
0.95,
0.94,
1.04,
0.75,
1,
1.28,
1.03,
1.17,
1.03,
]

voltage_noise_2 = [1.57,
1.66,
1.68,
2.15,
1.65,
1.68,
1.64,
1.93,
1.49,
1.63,
1.55,
1.83,
1.37,
1.6,
1.46,
1.56,
1.17,
1.49,
1.43,
1.59,
1.47,
1.59,
1.63,
1.69,
1.55,
1.65,
1.68,
1.65,
1.6,
2.08,
1.79,
1.7,
1.69,
1.57,
1.69,
1.65,
1.63,
1.57,
1.56,
1.54,
1.76,
1.56,
1.44,
1.76,
1.56,
1.64,
1.88,
1.67,
1.75,
1.74,
]

# kalman experiments
# # Experiment 1: Default values
# simple_kalman_filtered_1 = kalman_filter(voltage_noise, process_noise=0.01, measurement_noise=0.1)
# # Experiment 5: Larger measurement noise
# simple_kalman_filtered_3 = kalman_filter(voltage_noise, process_noise=0.01, measurement_noise=0.5)

# Moving average experiments
# experiment 1 : Window size = 5
# moving_avg_filtered_1 = moving_average_filter(voltage_noise, 5)
# # experiment 2 : Window size = 10
# moving_avg_filtered_2 = moving_average_filter(voltage_noise, 10)
# # experiment 3 : Window size = 15
# moving_avg_filtered_3 = moving_average_filter(voltage_noise, 15)

# # Median experiments
# # experiment 1 : Window size = 5
# median_filtered_1 = median_filter(voltage_noise, 5)
# # experiment 2 : Window size = 10
# median_filtered_2 = median_filter(voltage_noise, 10)
# # experiment 3 : Window size = 15
# median_filtered_3 = median_filter(voltage_noise, 15)

# EMA filter experiments
# ema_filtered_1 = exponential_moving_average_filter(voltage_noise, alpha=0.1)
# ema_filtered_2 = exponential_moving_average_filter(voltage_noise, alpha=0.15)
# ema_filtered_3 = exponential_moving_average_filter(voltage_noise, alpha=0.2)

# # Define the parameters for the pykalman filter
# transition_matrix = [[1, 1], [0, 1]]
# measurement_matrix = [[1, 0]]
# initial_state_mean = [0, 0]
# measurement_noise_covariance = [[0.1]]
# process_noise_covariance = [[0.01, 0.01], [0.01, 0.02]]

# # Apply the pykalman Kalman filter
# pykalman_filtered = pykalman_filter(voltage_noise, transition_matrix, measurement_matrix, initial_state_mean,
#                                     measurement_noise_covariance, process_noise_covariance)


# Apply filters to the sample data
moving_avg_window = 10
median_window = 10

moving_avg_filtered_1 = moving_average_filter(voltage_noise_1, moving_avg_window)
ema_filtered_1 = exponential_moving_average_filter(voltage_noise_1, alpha=0.15)
median_filtered_1 = median_filter(voltage_noise_1, median_window)
kalman_filtered_1 = kalman_filter(voltage_noise_1, process_noise=0.01, measurement_noise=0.5)

moving_avg_filtered_2 = moving_average_filter(voltage_noise_2, moving_avg_window)
ema_filtered_2 = exponential_moving_average_filter(voltage_noise_2, alpha=0.15)
median_filtered_2 = median_filter(voltage_noise_2, median_window)
kalman_filtered_2 = kalman_filter(voltage_noise_2, process_noise=0.01, measurement_noise=0.5)




#print(len(median_filtered))
# Plot the original and filtered data
plt.figure(figsize=(10, 6))
#plt.plot(time, voltage, label='Original')
plt.plot(time, voltage_noise_1, label='Original with noise')
plt.plot(time, moving_avg_filtered_1, label='Moving Average, Window size = 10')
plt.plot(time, ema_filtered_1, label='Exponential Moving Average')
plt.plot(time, median_filtered_1, label='Median Filter, Window size = 10')
plt.plot(time, kalman_filtered_1, label='Kalman Filter (process_noise=0.01, measurement_noise=0.5)')
plt.xlabel('Sample')
plt.ylabel('Voltage')
plt.title('ADC Voltage Filtering - Final filter comparison Experiments - Test 3 loadcell 1')
plt.legend()
plt.grid(True)
plt.show()

#print(len(median_filtered))
# Plot the original and filtered data
plt.figure(figsize=(10, 6))
#plt.plot(time, voltage, label='Original')
plt.plot(time, voltage_noise_2, label='Original with noise')
plt.plot(time, moving_avg_filtered_2, label='Moving Average, Window size = 10')
plt.plot(time, ema_filtered_2, label='Exponential Moving Average')
plt.plot(time, median_filtered_2, label='Median Filter, Window size = 10')
plt.plot(time, kalman_filtered_2, label='Kalman Filter (process_noise=0.01, measurement_noise=0.5)')
plt.xlabel('Sample')
plt.ylabel('Voltage')
plt.title('ADC Voltage Filtering - Final filter comparison Experiments - Test 3 loadcell 2')
plt.legend()
plt.grid(True)
plt.show()



Actual_weight = 3.950

V1_ave = calculate_average(voltage_noise_1)
V2_ave = calculate_average(voltage_noise_2)
raw_weight = calculate_weight(V1_ave,V2_ave)

V1_MA = calculate_average(moving_avg_filtered_1)
V2_MA = calculate_average(moving_avg_filtered_2)
MA_weight = calculate_weight(V1_MA, V2_MA)

V1_med = calculate_average(median_filtered_1)
V2_med = calculate_average(median_filtered_2)
med_weight = calculate_weight(V1_med, V2_med)


V1_EMA = calculate_average(ema_filtered_1)
V2_EMA = calculate_average(ema_filtered_2)
EMA_weight = calculate_weight(V1_EMA, V2_EMA)

V1_kalman = calculate_average(kalman_filtered_1)
V2_kalman = calculate_average(kalman_filtered_2)
Kalman_weight = calculate_weight(V1_kalman, V2_kalman)

#print("Actual weight: ",Actual_weight,"; Raw weight: ", raw_weight,"; Moving Average weight: ",MA_weight,"; Median weight: ",med_weight,"; Exponential MA weight: ",EMA_weight,"; Kalman weight: ",Kalman_weight)

print(f"Actual weight: {Actual_weight:.3f}; Raw weight: {raw_weight:.3f}; Moving Average weight: {MA_weight:.3f}; Median weight: {med_weight:.3f}; Exponential MA weight: {EMA_weight:.3f}; Kalman weight: {Kalman_weight:.3f}")


raw_weights = [3.959, 3.832, 3.992]
moving_avg_weights = [3.930, 3.827, 3.983]
ema_weights = [3.980, 3.785, 3.968]
kalman_weights = [3.931, 3.825, 3.988]
median_weights = [4.044, 3.981, 3.961]





known_weight = 3.950

# Calculate the MAE for each filter and the raw voltages
raw_mae = sum(abs(weight - known_weight) for weight in raw_weights) / len(raw_weights)
moving_avg_mae = sum(abs(weight - known_weight) for weight in moving_avg_weights) / len(moving_avg_weights)
ema_mae = sum(abs(weight - known_weight) for weight in ema_weights) / len(ema_weights)
kalman_mae = sum(abs(weight - known_weight) for weight in kalman_weights) / len(kalman_weights)
median_mae = sum(abs(weight - known_weight) for weight in median_weights) / len(median_weights)

# Calculate the standard deviation for each filter and the raw voltages
raw_std = np.std(raw_weights)
moving_avg_std = np.std(moving_avg_weights)
ema_std = np.std(ema_weights)
kalman_std = np.std(kalman_weights)
median_std = np.std(median_weights)

# Print the MAE and standard deviation values
print(f"Raw Voltages - MAE: {raw_mae:.4f}, Standard Deviation: {raw_std:.4f}")
print(f"Moving Average - MAE: {moving_avg_mae:.4f}, Standard Deviation: {moving_avg_std:.4f}")
print(f"EMA - MAE: {ema_mae:.4f}, Standard Deviation: {ema_std:.4f}")
print(f"Kalman Filter - MAE: {kalman_mae:.4f}, Standard Deviation: {kalman_std:.4f}")
print(f"Median Filter - MAE: {median_mae:.4f}, Standard Deviation: {median_std:.4f}")